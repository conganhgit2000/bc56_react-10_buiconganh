import React from "react";
import ItemChair from "./ItemChair";
import { useSelector } from "react-redux";

export default function HangGhe() {
  let chairListArr = useSelector((state) => state.chairListArr);
  let renderListChair = () => {
    return chairListArr.map((item, index) => {
      return <ItemChair item={item} key={index} />;
    });
  };
  return (
    <div className="mt-4 w-75 mx-auto list__chair">{renderListChair()}</div>
  );
}
