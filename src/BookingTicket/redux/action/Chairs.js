import { ADD_CHAIR, REMOVE_CHAIR } from "../constant/Chairs";

export let addChairAction = (chair, index) => {
  return {
    type: ADD_CHAIR,
    payload: {
      chair: chair,
      index: index,
    },
  };
};

export let removeChairAction = (chair) => {
  return {
    type: REMOVE_CHAIR,
    payload: chair,
  };
};
