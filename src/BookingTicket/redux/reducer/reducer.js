import { ADD_CHAIR, REMOVE_CHAIR, SET_DATA } from "../constant/Chairs";

const initialState = {
  chairListArr: [],
  activeIndex: null,
  activeItem: null,
  cart: [],
};

export let BookingTicketReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_DATA: {
      state.chairListArr = payload;
      return { ...state };
    }
    case ADD_CHAIR: {
      let cloneCart = [...state.cart];
      let newChair = { ...payload.item };
      let newActiveItem = payload.item.soGhe;
      cloneCart.push(newChair);
      return {
        ...state,
        cart: cloneCart,
        activeIndex: payload.index,
        activeItem: newActiveItem,
      };
    }
    case REMOVE_CHAIR: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.soGhe === payload);
      cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }
    default:
      return state;
  }
};
