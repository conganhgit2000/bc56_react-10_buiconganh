import React, { useEffect } from "react";
import HangGhe from "./HangGhe";
import ThongTinDatVe from "./ThongTinDatVe";
import { useDispatch } from "react-redux";
import axios from "axios";
import { SET_DATA } from "./redux/constant/Chairs";

export default function BookingTicket() {
  let dispatch = useDispatch();
  useEffect(() => {
    axios({
      url: "https://64c8b67ca1fe0128fbd61a36.mockapi.io/project",
      method: "GET",
    })
      .then((res) => {
        let action = {
          type: SET_DATA,
          payload: res.data,
        };
        dispatch(action);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  
  return (
    <div className="bgBookingMovie">
      <div className="content container-fluid">
        <div className="row">
          <div className="col-8 content__left">
            <h1 className="text-warning mt-4">ĐẶT VÉ XEM PHIM CYBERLEARN.VN</h1>
            <h6 className="text-light mb-2 font-weight-bold">Màn hình</h6>
            <div className="screen m-auto"></div>
            <HangGhe />
          </div>
          <div className="col-4 content__right">
            <h1 className="text-light mt-4">DANH SÁCH GHẾ BẠN CHỌN</h1>
            <ThongTinDatVe />
          </div>
        </div>
      </div>
    </div>
  );
}
