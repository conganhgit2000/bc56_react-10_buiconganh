import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { ADD_CHAIR } from "./redux/constant/Chairs";

export default function ItemChair({ item }) {
  let activeIndex = useSelector((state) => state.activeIndex);
  let activeItem = useSelector((state) => state.activeItem);
  let dispatch = useDispatch();
  let handleAddChair = (item, index) => {
    let action = {
      type: ADD_CHAIR,
      payload: {
        item: item,
        index: index,
      },
    };
    dispatch(action);
  };

  let { hang, danhSachGhe } = item;

  let renderChair = () => {
    return danhSachGhe.map((item, index) => {
      return (
        <div
          className={
            index === activeIndex && item.soGhe === activeItem
              ? "active my-2 number__chair"
              : "my-2 number__chair"
          }
          onClick={() => {
            handleAddChair(item, index);
          }}
        >
          {item.soGhe}
        </div>
      );
    });
  };

  return (
    <div className="d-flex justify-content-between text-light">
      <div className="text-warning font-weight-bold mt-3 mr-3 row__chair">
        {hang}
      </div>
      {renderChair()}
    </div>
  );
}
