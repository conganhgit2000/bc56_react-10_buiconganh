import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { REMOVE_CHAIR } from "./redux/constant/Chairs";

export default function ThongTinDatVe() {
  let cart = useSelector((state) => state.cart);
  let dispatch = useDispatch();
  let handleRemoveChair = (item) => {
    let action = {
      type: REMOVE_CHAIR,
      payload: item,
    };
    dispatch(action);
  };
  let renderTbody = () => {
    return cart.map((item) => {
      return (
        <tr>
          <th scope="row">{item.soGhe}</th>
          <td>{item.gia}</td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => {
                handleRemoveChair(item.soGhe);
              }}
            >
              X
            </button>
          </td>
        </tr>
      );
    });
  };

  let renderTotalPrice = () => {
    let totalPrice = 0;
    cart.map((item) => {
      totalPrice += item.gia;
      return (document.querySelector(".total__price").innerHTML =
        totalPrice.toLocaleString() + "VNĐ");
    });
  };

  return (
    <div>
      <div className="chair_note">
        <div className="note_item d-flex align-items-center">
          <div
            className="mr-4 chair_color"
            style={{ backgroundColor: "#bf1313" }}
          ></div>
          <span
            className="font-weight-bold text-light"
            style={{ fontSize: "26px" }}
          >
            Ghế đã đặt
          </span>
        </div>
        <div className="note_item d-flex align-items-center mt-2">
          <div
            className="mr-4 chair_color"
            style={{ backgroundColor: "#34bf13" }}
          ></div>
          <span
            className="font-weight-bold text-light"
            style={{ fontSize: "26px" }}
          >
            Ghế đang chọn
          </span>
        </div>
        <div className="note_item d-flex align-items-center mt-2">
          <div
            className="mr-4 chair_color"
            style={{ backgroundColor: "#ffffff" }}
          ></div>
          <span
            className="font-weight-bold text-light"
            style={{ fontSize: "26px" }}
          >
            Ghế đã đặt
          </span>
        </div>
      </div>
      <table className="table mt-4 font-weight-bold">
        <thead>
          <tr>
            <th scope="col" className="text-light">
              Số ghế
            </th>
            <th scope="col" className="text-light">
              Giá vé
            </th>
            <th scope="col" className="text-light">
              Hủy vé
            </th>
          </tr>
        </thead>
        <tbody>
          {renderTbody()}
          <tr>
            <th scope="row" className="text-light">
              Tổng tiền
            </th>
            <td className="total__price">{renderTotalPrice()}</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
