import BookingTicket from "./BookingTicket/BookingTicket";
import "./BookingTicket/style.css";

function App() {
  return (
    <div className="App">
      <BookingTicket />
    </div>
  );
}

export default App;
